#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from std_msgs.msg import Float32
from sensor_msgs.msg import Joy
from std_srvs.srv import *
import numpy as np

class JoyToCmdVel(object):
    def __init__(self):
        self.node_name = rospy.get_name()
        
        self.ps4_joystick = rospy.get_param("~ps4_joystick",False)
        self.low_speed_linear = rospy.get_param("~low_speed_linear",0.5)
        self.high_speed_linear = rospy.get_param("~high_speed_linear",1.0)
        self.low_speed_angular = rospy.get_param("~low_speed_angular",1.0)
        self.high_speed_angular = rospy.get_param("~high_speed_angular",4.0)
        self.vw_max = rospy.get_param("~max_angular_speed",2.0)
        
        self.manual = True
        self.new_vel = Twist()
        self.last_vel = Twist()
        self.last_last_vel = Twist()

        self.last_joy_stamp = None
        self.last_joy_stamp_timeout = rospy.get_param("~last_joy_stamp_timeout", 0.05)

        self.last_autonomous_stamp = None
        self.last_autonomous_stamp_timeout = rospy.get_param("~last_autonomous_stamp_timeout", 0.05)

        self.dt = 0.02
        self.max_accel_x = rospy.get_param("~max_accel_x",2.0)
        self.max_accel_w = rospy.get_param("~max_accel_w",2.0)
        # self.max_jerk_x = rospy.get_param("~max_jerk_x",1.0)
        self.max_dv = self.max_accel_x * self.dt
        self.max_dw = self.max_accel_w * self.dt
        # self.max_dax = self.max_jerk_x * self.dt

        self.pub = rospy.Publisher('~cmd_vel', Twist, queue_size=1)
        # self.pub_accel = rospy.Publisher('~cmd_accel', Float32, queue_size=1)
        # self.pub_jerk = rospy.Publisher('~cmd_jerk', Float32, queue_size=1)
        self.sub_joy = rospy.Subscriber("~joy", Joy, self.cbJoy, queue_size=1)
        self.sub_auto = rospy.Subscriber("~autonomous_cmd_vel",Twist,self.cbAutonomous,queue_size=1)

        self.timer = rospy.Timer(rospy.Duration(self.dt),self.cbLoop)
    
    def cbLoop(self,event):
        # Confirm that joystick was connected
        if self.last_joy_stamp is None:
            return
        if self.ps4_joystick:
            t_since_last_joystick_msg = (rospy.Time.now() - self.last_joy_stamp).to_sec()
            if t_since_last_joystick_msg > self.last_joy_stamp_timeout:
                rospy.logwarn("[joy_to_cmd_vel] Joystick was disconnected. Sending 0 velocity.")
                self.pubZeroCmdVel()
                return

        if not self.manual:
            if self.last_autonomous_stamp is None:
                rospy.loginfo("[joy_to_cmd_vel] Haven't received any autonomous cmd_vel msgs yet.")
                return
            t_since_last_autonomous_msg = (rospy.Time.now() - self.last_autonomous_stamp).to_sec()
            if t_since_last_autonomous_msg > self.last_autonomous_stamp_timeout:
                rospy.logwarn("[joy_to_cmd_vel] In autonomous mode, but haven't heard anything from planner in a while. Sending 0 velocity.")
                self.pubZeroCmdVel()
                return

        # As long as joystick is still sending commands properly,
        # smooth out the commanded velocity and publish to the motors
        smooth_vel = self.ramp(self.new_vel, self.last_vel, self.last_last_vel)
        self.pub.publish(smooth_vel)
        self.last_last_vel = self.last_vel
        self.last_vel = smooth_vel

    def ramp(self,new_vel,old_vel, old_old_vel):
        smooth_vel = Twist()


        #############################
        # Smooth Jerk
        ###########################
        # v0 = new_vel.linear.x
        # v_1 = old_vel.linear.x
        # v_2 = old_old_vel.linear.x

        # print "-----------"
        # print "v0: %.2f, v1: %2.f, v2: %.2f" %(v0, v_1, v_2)
        # desired_jerk = (v0 - 2*v_1 + v_2) / (self.dt*self.dt)
        # print "desired_jerk: %2.f" %(desired_jerk)
        # desired_accel = desired_jerk*self.dt + (v_1 - v_2) / self.dt
        # print "desired_accel: %2.f" %(desired_accel)
        # if abs(desired_accel) > self.max_accel_x:
        #     jerk_accel_limit = (np.sign(desired_accel)*self.max_accel_x - (v_1 - v_2) / self.dt) / self.dt
        # else:
        #     jerk_accel_limit = 10000
        # print "jerk_accel_limit: %2.f" %(jerk_accel_limit)

        # safe_jerk_magnitude = min(abs(desired_jerk), self.max_jerk_x, abs(jerk_accel_limit))
        # print "Jerk: desired: %.2f, max: %.2f, accel limit: %.2f" %(desired_jerk, self.max_jerk_x, jerk_accel_limit)
        # output = np.sign(desired_jerk)*safe_jerk_magnitude*self.dt*self.dt + 2*v_1 - v_2
        # print "output: %.2f" %(output)
        # smooth_vel.linear.x = np.sign(desired_jerk)*safe_jerk_magnitude*self.dt*self.dt + 2*v_1 - v_2

        # self.pub_accel.publish(Float32(data=desired_accel))
        # self.pub_jerk.publish(Float32(data=np.sign(desired_jerk)*safe_jerk_magnitude))

        ################################################


        diff_x = new_vel.linear.x - old_vel.linear.x
        if diff_x > self.max_dv or diff_x < - self.max_dv:
            # too much change, need to ramp
            smooth_vel.linear.x = old_vel.linear.x + np.sign(diff_x)*self.max_dv
        else:
            smooth_vel.linear.x = new_vel.linear.x

        diff_w = new_vel.angular.z - old_vel.angular.z
        if diff_w > self.max_dw or diff_w < - self.max_dw:
            smooth_vel.angular.z = old_vel.angular.z + np.sign(diff_w)*self.max_dw
        else:
            smooth_vel.angular.z = new_vel.angular.z

        smooth_vel.angular.z = np.clip(smooth_vel.angular.z, -self.vw_max, self.vw_max)
        smooth_vel.linear.x = np.clip(smooth_vel.linear.x, -self.high_speed_linear, self.high_speed_linear)

        return smooth_vel

    def pubZeroCmdVel(self):
        self.new_vel = Twist()
        self.last_vel = Twist()
        self.pub.publish(Twist())

    def cbJoy(self,data):
        if data.header.seq == 0: # first joystick msg is usually garbage
            return
        self.last_joy_stamp = data.header.stamp
        if self.ps4_joystick:
            # PS4 Joystick
            green = data.buttons[1] # x
            red = data.buttons[2] # o
            blue = data.buttons[0] # square
            yellow = data.buttons[3] # triangle
            spin = data.buttons[8] 
            slow_trigger = data.buttons[6] 
            fast_trigger = data.buttons[5] #R1
            forward = data.axes[1]
            turn = data.axes[2]

        else:
            # Matricom Joystick
            green = data.buttons[0]
            red = data.buttons[1]
            blue = data.buttons[3]
            yellow = data.buttons[4]
            spin = data.buttons[8]
            slow_trigger = data.buttons[6]
            fast_trigger = data.buttons[7]
            forward = data.axes[1]
            turn = data.axes[2]

        if blue==1:
            self.manual = True
            rospy.loginfo('Rover in manual control mode')
        if yellow==1:
            self.manual = False
            rospy.loginfo('Rover in autonomous control mode')

        if red==1:
            rospy.loginfo('Rover in manual control mode - red button pressed')
            self.manual = True
            self.pubZeroCmdVel()

        if self.manual:
            if fast_trigger:
                linear_speed = self.high_speed_linear
                angular_speed = self.high_speed_angular
            else:
                linear_speed = self.low_speed_linear
                angular_speed = self.low_speed_angular

            cmd_vel = Twist()
            vx = 0
            if abs(forward)>=0.1:
                vx = linear_speed*forward
            vw = 0
            if abs(turn)>=0.1:
                vw = angular_speed*turn
            cmd_vel.linear.x = vx
            cmd_vel.angular.z = vw
            self.new_vel = cmd_vel

    def cbAutonomous(self,data):
        self.last_autonomous_stamp = rospy.Time.now()
        if not self.manual:
            self.new_vel = data

    def on_shutdown():
        rospy.loginfo("[%s] shutting down.\n"%self.node_name)
        pass

if __name__ == '__main__':
    # Initialize the node with rospy
    rospy.init_node('joy_to_cmd_vel_node', anonymous=False)
    node = JoyToCmdVel()
    # Setup proper shutdown behavior 
    rospy.on_shutdown(node.on_shutdown)
    # Keep it spinning to keep the node alive
    rospy.spin()
